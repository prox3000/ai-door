#include "Camera.h"
#include "Intersection/jsonIO.h"
#define  NOW time(NULL)

extern bool isOpenDoor;
extern int state_door;
time_t time_open=NOW;

char prev_state_door=0; 
time_t time_open_isOpenDoor=NOW;


Camera::Camera(Config_params cam_params, Config_params_all _params_all)
	: frames(cam_params.camID, cam_params.prewrite, cam_params.postwrite)
	//, face_detector_from_neuron("data/yolo-face.cfg", "data/yolo-face_final.weights", 0)
{
	init_face_detect();
	//init_eyes_detect();
	capture = cv::VideoCapture(cam_params.cam_url);
	if (!capture.isOpened())
	{
		std::cerr << "ERROR: unable to open camera stream" << std::endl;
	}
	std::cout << "Camera " << cam_params.cam_url << std::endl;

	//capture.set(cv::CAP_PROP_FPS, 22);
	fps = 22;//capture.get(cv::CAP_PROP_FPS);
	std::cout << "fps: " << fps << std::endl;
	resolution.width = static_cast<int>(capture.get(CV_CAP_PROP_FRAME_WIDTH));
	resolution.height = static_cast<int>(capture.get(CV_CAP_PROP_FRAME_HEIGHT));
	//---Setting cropped frame ROI---
	cam_params.roi.x *= resolution.width / 100;
	cam_params.roi.y *= resolution.height / 100;
	cam_params.roi.width *= resolution.width / 100;
	cam_params.roi.height *= resolution.height / 100;
	//Do this only after cropped frame ROI
	//is converted from percents to real coordinates
	camera_params = cam_params;
	params_all = _params_all;
	//=======================
	croppedFrameSize = cv::Size(cam_params.roi.width, cam_params.roi.height);
	float scale = 1.0;
	if (cam_params.roi.width > defaultSize.width ||
		cam_params.roi.height > defaultSize.height)
	{
		float xScale = static_cast<float>(defaultSize.width) / cam_params.roi.width;
		float yScale = static_cast<float>(defaultSize.height) / cam_params.roi.height;
		scale = std::min(xScale, yScale);
		croppedFrameSize.width *= scale;
		croppedFrameSize.height *= scale;
	}
	frames.debugSetFrameSize(croppedFrameSize);

	auto camPolygons = readCamPoly("data/config.json", cam_params.camID);
	for (auto & poly : camPolygons.polygons)
	{
		std::cout << "Poly: " << poly.first << ": ";
		for (auto & p : poly.second)
		{
			//this is for making polygons appropriate for all resolutions of the same camera
			/*p.x *= float(resolution.width) / camPolygons.resolution.width;
			p.y *= float(resolution.height) / camPolygons.resolution.height;*/
			p = shiftAndScale(p, cv::Point(-cam_params.roi.x, -cam_params.roi.y), scale);
			std::cout << p;
		}
		std::cout << std::endl;
	}
	pISDetector = std::make_unique<IntersectionDetector>(camPolygons.polygons);
	pIntersectionAnalyzer = std::make_unique<IntersectionAnalyzer>(IntersectionAnalyzer::load(cam_params.location));
}

void Camera::init_face_detect()
{
	//std::string face_cascade_name = "/home/startup/opencv/data/haarcascades/haarcascade_frontalface_alt.xml";
	//std::string face_cascade_name = "/home/startup/opencv/data/haarcascades/haarcascade_frontalcatface_extended.xml";
	std::string face_cascade_name = "haarcascade_frontalface_alt2.xml";
	
	//std::string face_cascade_name = "haarcascade_frontalface_default.xml";
	face_detector.load(face_cascade_name);
	if (!face_detector.load(face_cascade_name))
	{
		std::cout << "error loading cascade" << std::endl;
	}
}

void Camera::init_eyes_detect()
{
	
	std::string eyes_cascade_name = "/home/startup/opencv/data/haarcascades/haarcascade_eye_tree_eyeglasses.xml";
	eyes_detector.load(eyes_cascade_name);
	if (!eyes_detector.load(eyes_cascade_name))
	{
		std::cout << "error loading cascade" << std::endl;
	}
}

void Camera::start_processing()
{
	p_frame_reader = std::make_unique<std::thread>([this]()
	{
		while (1)
		{
			cv::Mat frame;
			int emptyFrames = 0;
			while (frame.empty())
			{
				try
				{
					//std::cerr<<camera_params.camID << " 1" << std::endl;
					capture >> frame;
					//std::cerr<<camera_params.camID << " 2" << std::endl;
					emptyFrames++;
					if (emptyFrames > 10)
					{
						std::cout << "Recapturing..." << std::endl;
						capture.release();
						capture.open(camera_params.cam_url);
						capture >> frame;
						emptyFrames = 0;
					}
				}
				catch (const std::exception& ex)
				{
					log(std::string("Exception caught while reading frame: ") + ex.what());
				}
				//std::cerr<<camera_params.camID << " 3" << std::endl;
			}
			std::cerr << camera_params.camID << " before putFrame\n";
			cv::waitKey(50);
			//std::cerr<<camera_params.camID << " 4" << std::endl;
			frames.putFrame(frame);
			//std::cerr<<camera_params.camID << " 5" << std::endl;
			cv::waitKey(10);
			std::cerr << camera_params.camID << " after putFrame\n";
		}
	});

	p_frame_processor = std::make_unique<std::thread>([this]()
	{
		std::cout << "Processing\n";
		cv::Mat frame, prevFrame, part;
		while (true)
		{
			frame = frames.getFrame();
			part = frame(cv::Rect(camera_params.roi)).clone();
			cv::resize(part, part, croppedFrameSize);
			cv::imwrite("debug/lastFrame_" + camera_params.camID + ".jpg", part);

			if (prevFrame.empty())
			{
				prevFrame = part.clone();
				//part.copyTo(prevFrame);
				continue;
			}

			bool movementDetected = false;
			if (part.size() == prevFrame.size() &&
				part.type() == prevFrame.type())
			{
				movementDetected = move_detect(part.clone(), prevFrame.clone());
			}

			//if (movementDetected == true)
			//{
			//	std::cout << "Movement\n";
			//}
			
			prevFrame = part.clone();

			std::cerr << camera_params.camID << "Heads\n";
			auto heads = BoxToRect(pISDetector->detect(part));
			//bool movementDetected = !heads.empty();
			std::vector<cv::Rect> faces;
			for (auto head : heads)
			{
				head.x = std::max(0, head.x - head.width / 4);
				head.width = std::min(part.cols, head.width + head.width / 4);
				//to full frame roi
				head = shiftAndScale(head, cv::Point(0, 0),
						static_cast<float>(camera_params.roi.width) / croppedFrameSize.width);
				//to full frame
				head = shiftAndScale(head, cv::Point(camera_params.roi.x, camera_params.roi.y), 1);
				
				if (!(0 <= head.x && 0 <= head.width && head.x + head.width <= frame.cols &&
					0 <= head.y && 0 <= head.height && head.y + head.height <= frame.rows))
				{
					std::cerr << head << std::endl;
					std::cerr << frame.size() << std::endl;
					std::cerr << "error getting head from full frame\n";
				}
				head = adjustROI(head, frame.size());
				auto temp_faces = getFaces(frame(head));
				for (auto & face : temp_faces)
				{
					face += head.tl();
					faces.push_back(face);
				}
			}

			std::cerr << camera_params.camID << "Faces\n";
			std::vector<NamedFace> namedFaces;
			for (auto face : faces)
			{
					if(camera_params.location=="Outside")			
						AddBorder(frame,face,5,40);
					if(camera_params.location=="Inside")
						AddBorder_without_removing(frame,face,1,10);
					
					NamedFace namedFace;
					//to scaled roi
					namedFace.face = shiftAndScale(face, cv::Point(-camera_params.roi.x, -camera_params.roi.y),
						static_cast<float>(croppedFrameSize.width) / camera_params.roi.width);
					namedFace.img=frame;
					face = adjustROI(face, frame.size());
					namedFace.img_face=frame(face);
					std::string temp=SendImg(namedFace.img_face,params_all);
					if (std::find(unrecognize.begin(), unrecognize.end(), temp) == unrecognize.end())
					{
						namedFace.name_face=temp;
						SaveImg(namedFace.img_face, true, temp);
					}
					else
					{
						namedFace.name_face="UNRECOGNIZED";
						SaveImg(namedFace.img_face, false, temp);
					}
					
					std::cerr << "There is " + namedFace.name_face << std::endl;
					namedFaces.push_back(namedFace);				
			}


			if (movementDetected ||	!faces.empty())
			{
				frames.startWriting(fps, resolution);
			}
			else
			{
				frames.stopWriting();
			}

			if (frames.isWriting())
			{
				std::cerr << camera_params.camID << ": movement or faces detected\n";
				auto & tracks = pISDetector->detectIntersections(namedFaces);
				std::cerr << camera_params.camID << ": After detectIntersections\n";
				for (auto & track : tracks)
				{
					for (auto intersection : track->intersections)
					{
						cv::line(part, intersection.s.start, intersection.s.end, cv::Scalar(255, 0, 0));
						cv::circle(part, intersection.s.end, 1, cv::Scalar(255, 0, 0), 2);
					}
				}
				if(pIntersectionAnalyzer->detectInOut(tracks))
				{
					std::cout << "Try open door" << std::endl;
					if(camera_params.odoor>0 && !isOpenDoor && true_period(params_all)&& state_door<2)
					{
						std::cout << "Open door" << std::endl;
						const char *a[]={"a",params_all.door.ip.c_str(),params_all.door.port.c_str()};
						OpenDoor( 3,a);
						isOpenDoor=true;
						prev_state_door=1;	
						time_open_isOpenDoor=NOW;
						log("Door opened");
					}
				}
				cv::Size bigFaceSize = scale(cv::Size(frame.cols * 0.05, frame.rows * 0.10),
					static_cast<float>(croppedFrameSize.width) / camera_params.roi.width);
				if (camera_params.event != "none")
				{
					pIntersectionAnalyzer->detectEventByFaceSize(namedFaces, bigFaceSize, camera_params.event);
				}

				if(isOpenDoor)
				{
					if(prev_state_door==1 && state_door>=2)
					{
						std::cout << "prev_state_door=2" << std::endl;
						time_open=NOW;
						prev_state_door=2;
					}
					else if(prev_state_door==2 && (NOW-time_open)>2)	
					{		
						std::cout << "isOpenDoor=false" << std::endl;
						isOpenDoor=false;
						prev_state_door=0;
					}
					if((NOW-time_open_isOpenDoor)>7)
					{
						std::cout << "isOpenDoor+=false" << std::endl;
						isOpenDoor=false;
						prev_state_door=0;
					}
				}
				
				std::cerr << "Put debug frame" << std::endl;
				frames.debugPutFrame(part);
			}
		}
	});
}

bool Camera::move_detect(const cv::Mat & image, const cv::Mat & foreground)
{
	cv::Mat imgDifference, gray;
	absdiff(image, foreground, imgDifference);
	IplImage* _image = new IplImage(imgDifference);
	
	IplImage* bin;
	cv::waitKey(1);
	assert(_image != 0);
	bin = cvCreateImage(cvGetSize(_image), IPL_DEPTH_8U, 1);
	cvConvertImage(_image, bin, CV_BGR2GRAY);
	cvCanny(bin, bin, 50, 200);
	CvMemStorage* storage = cvCreateMemStorage(0);
	CvSeq* contours = 0;
	int contours_count = cvFindContours(bin, storage, &contours, sizeof(CvContour), CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, cvPoint(0, 0));	
	
	cvtColor(image, gray, cv::COLOR_BGR2GRAY);
	
	for (CvSeq* current = contours; current != NULL; current = current->h_next)
	{
		CvPoint2D32f center;
		float radius = 0;
		
		cvMinEnclosingCircle(current, &center, &radius);
		if (radius > camera_params.sens)
		{
			//std::cout << "radius "<<radius<<" on "<<camera_params.camID<<"\n";
			//	cvCircle(_image, cvPointFrom32f(center), radius, CV_RGB(255, 0, 0), 1, 8);
			cvReleaseMemStorage(&storage);
			cvReleaseImage(&bin);
			delete(_image);
			return true;
			//double area = fabs(cvContourArea(current));
			//double perim = cvContourPerimeter(current);
			//cvDrawContours(_image, current, cvScalar(0, 0, 255), cvScalar(0, 255, 0), -1, 1, 8);
     
		}
	}
	
	cvReleaseMemStorage(&storage);
	cvReleaseImage(&bin);
	delete(_image);
	return false;
}

std::vector<cv::Rect> Camera::getFaces(const cv::Mat& image)
{
	std::vector<cv::Rect>faces;
	cv::Mat gray;
	cvtColor(image, gray, cv::COLOR_BGR2GRAY);
	face_detector.detectMultiScale(gray, faces, 1.1, 2, 0 | 
		//CV_HAAR_DO_CANNY_PRUNING,
		//CV_HAAR_DO_ROUGH_SEARCH,
		//CV_HAAR_FEATURE_MAX,
		CV_HAAR_FIND_BIGGEST_OBJECT,
		//CV_HAAR_SCALE_IMAGE,
		cv::Size(70,70));

	return faces;
}

bool Camera::getFaces_bool(const cv::Mat& image)
{
	std::vector<cv::Rect>faces;
	cv::Mat gray;
	cvtColor(image, gray, cv::COLOR_BGR2GRAY);
	face_detector.detectMultiScale(gray, faces, 1.05, 8, 0 | 
		//CV_HAAR_DO_CANNY_PRUNING,
		//CV_HAAR_DO_ROUGH_SEARCH,
		//CV_HAAR_FEATURE_MAX,
		CV_HAAR_FIND_BIGGEST_OBJECT,
		//CV_HAAR_SCALE_IMAGE,
		cv::Size(70,70));

	return faces.size();
	
}

bool Camera::getEyes(const cv::Mat& image)
{
	std::vector<cv::Rect>eyes;
	cv::Mat gray;
	cvtColor(image, gray, cv::COLOR_BGR2GRAY);
	eyes_detector.detectMultiScale(gray, eyes, 1.1, 3, 0|CV_HAAR_FIND_BIGGEST_OBJECT, cv::Size(0, 0));
	return eyes.size()?true:false;
}

/*std::vector<cv::Rect> Camera::getFaces(const cv::Mat& image)
{
	std::vector<cv::Rect>faces;
	cv::Mat gray;
	cvtColor(image, gray, cv::COLOR_BGR2GRAY);
	face_detector.detectMultiScale(gray, faces, 1.1, 3, 0|CV_HAAR_FIND_BIGGEST_OBJECT,
		cv::Size(camera_params.size_face, camera_params.size_face));

	return faces;
}*/

/*
std::vector<cv::Rect> Camera::getFaces_from_neuron(const cv::Mat& image)
{
	std::vector<cv::Rect>faces;
	
	boxes=face_detector_from_neuron.detect(image, 0.01);
	
	for (auto box : boxes)
	{
		cv::Rect objectRect(box.x, box.y, box.w, box.h);
		faces.push_back(objectRect);		
	}
	return faces;
}*/