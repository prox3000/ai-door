#pragma once
#include <iostream>
#include <vector>
#include <thread>
#include <memory>
#include "opencv2/opencv.hpp"

#include "FrameManager.h"
#include "Util.hpp"
#include "IntersectionAnalyzer.h"
#include "Intersection/intersection_detector.h"

#include "Intersection/yolo_v2_class.hpp"

class Camera
{
public:
	Camera(Config_params cam_params, Config_params_all _params_all);

	void init_face_detect();
	void init_eyes_detect();
	void start_processing();
	bool move_detect(const cv::Mat& image, const cv::Mat& foreground);
	std::vector<cv::Rect> getFaces(const cv::Mat& image);
	bool getFaces_bool(const cv::Mat& image);
	bool getEyes(const cv::Mat& image);
	
	//Detector face_detector_from_neuron;
	//std::vector<bbox_t> boxes;
	//std::vector<cv::Rect> getFaces_from_neuron(const cv::Mat& image);

	std::unique_ptr<std::thread> p_frame_processor;

private:
	Config_params  camera_params;
	Config_params_all params_all;
	cv::VideoCapture capture;
	std::unique_ptr<std::thread> p_frame_reader;
	std::unique_ptr<IntersectionDetector> pISDetector;
	std::unique_ptr<IntersectionAnalyzer> pIntersectionAnalyzer;
	cv::CascadeClassifier face_detector;
	cv::CascadeClassifier eyes_detector;
	FrameManager frames;

	double fps;
	cv::Size resolution;
	cv::Size croppedFrameSize;
};