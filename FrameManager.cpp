#include <thread>
#include <chrono>

#include "FrameManager.h"
#include "Intersection/Tuner.h"
#include "Util.hpp"


FrameManager::FrameManager(const std::string& name, int preWritingSize, int postWritingSize)
	: defaultName("Video/" + name + ".avi"),
	preWritingFrames(preWritingSize),
	postWritingFrameNumber(postWritingSize)
{
	std::cout << defaultName << std::endl;
}

bool FrameManager::startWriting(double fps, cv::Size size)
{
	if (!debugWriter.isOpened())
	{
		auto debugName = defaultName;
		debugName.replace(debugName.begin(), debugName.begin() + std::string("Video/").size(),
			"Video/DEBUG" + std::to_string(debugVideoCounter));
		debugWriter.open(debugName, CV_FOURCC_MACRO('M', 'P', '4', '2'), fps, debugFrameSize);
	}

	if (isWriting())
	{
		postWritingCounter = 0;
		return true;
	}

	std::cout << defaultName << " Start writing\n";
	if (++debugVideoCounter % 3 == 0)
	{
		std::cout << debugVideoCounter << std::endl;
		auto debugName = defaultName;
		debugName.replace(debugName.begin(), debugName.begin() + std::string("Video/").size(),
			"Video/DEBUG" + std::to_string(debugVideoCounter));
		debugWriter.open(debugName, CV_FOURCC_MACRO('M', 'P', '4', '2'), fps, debugFrameSize);
	}

	startTime = time(0);
	writer.open(defaultName, CV_FOURCC_MACRO('M', 'P', '4', '2'), fps, size);
	if (!writer.isOpened())
	{
		std::cerr << "Problem opening video writer.\n";
		return false;
	}
	std::cerr << "Start writing pre witing frames" << std::endl;

	preWritingMutex.lock();
	auto preWritingFramesCopy = std::move(preWritingFrames);
	preWritingMutex.unlock();

	while (preWritingFramesCopy.size_q() > 0 && writer.isOpened())
	{
		writer << preWritingFramesCopy.getFrame();
	}
	writing = true;
	std::cerr << "Pre witing done" << std::endl;
	
	return isWriting();
}

void FrameManager::stopWriting()
{
	if (!isWriting())
	{
		return;
	}
	if (postWritingCounter++ < postWritingFrameNumber)
	{
		return;
	}

	std::cout << defaultName << " Stop writing\n";
	postWritingCounter = 0;
	writing = false;
	if (writer.isOpened())
	{
		std::lock_guard<std::mutex> lock(writerMutex);
		writer.release();
	}

	endTime = time(0);
	const int BUFF_LEN = 25;
	char buff_start[BUFF_LEN], buff_end[BUFF_LEN];
	strftime(buff_start, BUFF_LEN, "%Y-%m-%d_%H-%M-%S", localtime(&startTime));
	strftime(buff_end, BUFF_LEN, "=%Y-%m-%d_%H-%M-%S", localtime(&endTime));
	std::string name("Video/");
	name += buff_start;
	name += buff_end;
	name += ".avi";
	rename(defaultName.c_str(), name.c_str());
}

void FrameManager::putFrame(cv::Mat frame)
{
	std::cerr << defaultName << ": 1" << std::endl;
	frames.addFrame(frame.clone());
	std::cerr << defaultName << ": 2" << std::endl;
	preWritingMutex.lock();
	preWritingFrames.addFrame(frame.clone());
	preWritingMutex.unlock();
	std::cerr << defaultName << ": 3" << std::endl;

	if (isWriting() && writer.isOpened())
	{
		std::lock_guard<std::mutex> lock(writerMutex);
		writer << frame;
	}
	std::cerr << defaultName << ": 4" << std::endl;
}

cv::Mat FrameManager::getFrame()
{
	cv::Mat frame;
	while ((frame = frames.getFrame()).empty())
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	frames.skip();
	return frame;
}

void FrameManager::debugPutFrame(cv::Mat frame)
{
	if (debugWriter.isOpened())
	{
		debugWriter << frame;
	}
}