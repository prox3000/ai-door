#pragma once
#include <iostream>
#include <ctime>
#include <mutex>
#include "opencv2/opencv.hpp"

#include "FrameQueue.h"


class FrameManager
{
public:
	FrameManager(const std::string& name, int preWritingSize, int postWritingSize);

	bool startWriting(double fps, cv::Size size);
	void stopWriting();
	void putFrame(cv::Mat frame);
	//always returns cv::Mat that is not empty
	cv::Mat getFrame();

	bool isWriting() const { return writing; }

	//---DEGUG---
	void debugPutFrame(cv::Mat frame);
	void debugSetFrameSize(cv::Size size) { debugFrameSize = size; }
	//===========

private:
	std::mutex writerMutex;
	std::mutex preWritingMutex;
	
	std::string defaultName;
	cv::VideoWriter writer;
	FrameQueue frames;
	FrameQueue preWritingFrames;
	const int postWritingFrameNumber;
	int postWritingCounter = 0;
	time_t startTime;
	time_t endTime;
	bool writing = false;

	//---DEGUG---
	cv::VideoWriter debugWriter;
	cv::Size debugFrameSize;
	size_t debugVideoCounter = 1;
	//===========
};