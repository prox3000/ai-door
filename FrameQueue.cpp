#include "FrameQueue.h"


FrameQueue::FrameQueue()
{
	frame_status = false;
	maxFrames = 100;
}

FrameQueue::FrameQueue(int a)
{
	frame_status = false;
	maxFrames = a;
}

FrameQueue::FrameQueue(FrameQueue&& fq)
{
	frame_status = fq.frame_status;
	maxFrames = fq.maxFrames;
	frames = std::move(fq.frames);//std::forward<std::queue<cv::Mat>>(fq.frames);
}

FrameQueue::~FrameQueue()
{
}

void FrameQueue::addFrame(cv::Mat frame)
{
	std::lock_guard<std::mutex> lock(m);
	frames.emplace(std::move(frame));
	if (frames.size() > maxFrames)
	{
		frames.pop();
	}
}

void FrameQueue::popFrame(int n)
{
	for (int i = 0; i < n; i++)
	{
		if (frames.empty())
		{
			return;
		}
		frames.pop();
	}
}

int FrameQueue::size_q()
{
	return frames.size();
}

cv::Mat FrameQueue::getFrame()
{
	std::lock_guard<std::mutex> lock(m);
	if (frames.empty())
	{
		return cv::Mat();
	}
	cv::Mat frame = std::move(frames.front());
	frames.pop();

	return frame;
}


void FrameQueue::clear_q() {
	any_clear(frames);
}


void FrameQueue::skip()
{
	std::lock_guard<std::mutex> lock(m);
	clear_q();
}