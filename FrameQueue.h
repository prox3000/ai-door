#pragma once
#include <queue>
#include <mutex>
#include "opencv2/opencv.hpp"

#include "Util.hpp"

	
class FrameQueue
{
public:
	bool frame_status;
	int maxFrames;

	FrameQueue();
	FrameQueue(int a);
	FrameQueue(FrameQueue&& fq);
	~FrameQueue();

	void clear_q();

	void addFrame(cv::Mat frame);
	void popFrame(int n);
	cv::Mat getFrame();
	void skip();
	int size_q();
private:
	std::queue<cv::Mat> frames;
	std::mutex m;
};