#include "DBAccessor.hpp"

DBAccessor::DBAccessor(const Config & config)
	: connected(false)
{
	connection = mysql_init(NULL);
	if (!connection) 
	{
		connected = false;
		return;
	}
	connected = mysql_real_connect(connection,
		config.host.c_str(),
		config.login.c_str(),
		config.password.c_str(),
		config.dbName.c_str(),
		0, NULL, 0);
	if (!connected)
	{
		
	}
}


DBAccessor::~DBAccessor()
{
	mysql_close(connection);
}


long DBAccessor::execDirect(const std::string & query)
{
	auto ret = mysql_query(connection, query.c_str());
	if (ret != 0)
	{
		std::cerr << mysql_error(connection);
	}
	else if (query.find("INSERT") != std::string::npos)
	{
		return mysql_insert_id(connection);
	}
	return ret;
}

std::string DBAccessor::getLastEventByName(const std::string& name)
{
	std::string query = "SELECT event FROM Entry WHERE name='" + name + "' ORDER BY id DESC LIMIT 1;";
	if (mysql_query(connection, query.c_str()))
	{
		return std::string();
	}

	MYSQL_RES *result = mysql_store_result(connection);
	if (result == NULL) 
	{
		return std::string();
	}

	std::string event;
	MYSQL_ROW row = mysql_fetch_row(result);
	event = row[0];

	mysql_free_result(result);

	return event;
}