#pragma once
#define int64 mysqlInt64
#define uint64 mysqlUInt64
#include <string.h>
#include <iostream>
#include <mysql.h>
#include <my_global.h>
#undef int64
#undef uint64

class DBAccessor
{
public:
	struct Config
	{
		std::string host;
		std::string login;
		std::string password;
		std::string dbName;
	};

	DBAccessor(const Config & config);
	~DBAccessor();

	bool isConnected() const { return connected; }
	long execDirect(const std::string & query);
	std::string getLastEventByName(const std::string& name);

private:
	MYSQL * connection;
	bool connected;
};
