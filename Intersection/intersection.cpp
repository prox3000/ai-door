#include <string>
#include <algorithm>

#include "intersection.h"
#include "DBAccessor.hpp"
#include "../Util.hpp"

std::vector<Intersection> detectIntersection(IntersectionData & idata, cv::Point2f p)//CRegion reg)
{
	std::cerr << "detectIntersection start\n";
	std::vector<Intersection> intersections;

	using cv::Point;
	Point keyPoint = p;//(reg.m_rect.x + reg.m_rect.width / 2, reg.m_rect.y + reg.m_rect.height);

	if (std::any_of(idata.oldPoints.begin(), idata.oldPoints.end(), [](Point & p) { return p == Point(-1, -1); }))
	{
		std::replace(idata.oldPoints.begin(), idata.oldPoints.end(), Point(-1, -1), keyPoint);
		return intersections;
	}

	std::vector<LastPointOutOfPolygonBufferInfo> oldPointsInfo(idata.polygons.size(), { true, 0 });
	//std::vector<bool> oldPointRefreshNeeded(idata.polygons.size(), true);//to know if we need to refresh oldPoints
	//oldPointRefreshNeeded.resize(idata.polygons.size(), true);

	idata.newPoint = keyPoint;
	size_t i = 0;
	for (auto poly : idata.polygons)
	{
		Intersection::Direction direction;
		size_t intersectionNumber = 0;
		for (int j = 0; j < poly.second.size() - 1; j++)
		{
			auto polygonSide = Segment{ poly.second[j], poly.second[j + 1] };
			//if object is close to line we do not refresh old point
			if (distPointToSegment(polygonSide, idata.newPoint) < LINE_BUFFER_DISTANCE)
			{
				oldPointsInfo[i].refreshNeeded = false;
				intersectionNumber = 0;
				break;
			}
			else if (areSegmentsCrossed(polygonSide, Segment{ idata.oldPoints[i], idata.newPoint }))
			{
				direction = isPointRightOfLine(polygonSide, idata.newPoint) ? 
					Intersection::Direction::Left : Intersection::Direction::Right;

				intersectionNumber++;
			}
		}
		if (intersectionNumber % 2 == 1)
		{
			Segment s{ idata.oldPoints[i], idata.newPoint };
			intersections.push_back({ poly.second, poly.first, direction, s, oldPointsInfo[i].time });
		}
		i++;
	}

	for (int i = 0; i < idata.oldPoints.size(); i++)
	{
		if (oldPointsInfo[i].refreshNeeded)
		{
			idata.oldPoints[i] = idata.newPoint;
			oldPointsInfo[i].time = time(0);
		}
	}

	std::sort(intersections.begin(), intersections.end(), [](Intersection l, Intersection r)
	{
		return l.timeOfFirstPoint < r.timeOfFirstPoint;
	});
	std::cerr << "detectIntersection end\n";
	return intersections;
}
