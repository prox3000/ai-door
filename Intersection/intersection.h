#pragma once

#include <opencv2/core.hpp>

#include "defines.h"
#include "mymath.h"
#include "DBAccessor.hpp"

//distance to line in pixels in which intersection is not counted
const size_t LINE_BUFFER_DISTANCE = 25;

struct Intersection
{
	enum Direction { Left, Right };
	
	std::vector<cv::Point> polygon;
	std::string polygonID;
	Direction direction;
	Segment s;
	time_t timeOfFirstPoint;
};

struct IntersectionData
{
	std::map<std::string, std::vector<cv::Point>> polygons;
	std::vector<cv::Point> oldPoints;
	cv::Point newPoint;
	std::string * name;
};

struct LastPointOutOfPolygonBufferInfo
{
	bool refreshNeeded;
	time_t time;
};

std::vector<Intersection> detectIntersection(IntersectionData & idata, cv::Point2f p);//CRegion reg);
