#include <ctime>

#include "intersection_detector.h"

IntersectionDetector::IntersectionDetector(const std::map<std::string,std::vector<cv::Point>>& polygons)
	: tracker(true, kalmanStep, accelNoise, distThresh, skipFrames, maxTraceLength),
	//detector("data/yolo-voc.cfg", "data/yolo-voc.weights", 0)
	detector("data/head/yolo-obj.cfg", "data/head/yolo-obj_1000.weights", 0)
{
	tracker.polygons = polygons;
}

std::vector<bbox_t> IntersectionDetector::detect(cv::Mat frame, float thresh)
{
	lastFrame = frame;
	try
	{
		lastDetects = detector.detect(frame, thresh);
	}
	catch (const std::exception & ex)
	{
		std::cout << ex.what() << std::endl;
	}
	joinOverlapped();

	return lastDetects;
}

const tracks_t& IntersectionDetector::detectIntersections(const std::vector<NamedFace> & faces)
{
	for (auto poly : tracker.polygons)
	{
		for (int i = 0; i < poly.second.size() - 1; i++)
		{
			cv::line(lastFrame, poly.second[i], poly.second[i + 1], cv::Scalar(255, 255, 0));
			cv::putText(lastFrame, poly.first, poly.second[i + 1], cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 0));
		}
	}
	for (auto face : faces)
	{
		cv::rectangle(lastFrame, face.face, cv::Scalar(0, 0, 255));
	}

	std::map<size_t, NamedFace> correlation;
	if (!faces.empty())
	{
		correlation = correlateFacesWithObjects(lastDetects, faces);
	}
	std::cerr << "Correlation size: " + std::to_string(correlation.size()) << std::endl;
	std::vector<Point_t> centers;
	regions_t regions;
	for (auto box : lastDetects)
	{
		if (box.h > box.w * 1.8)
		{
			auto croppedHeight = box.h / 5;
			box.y += croppedHeight / 2;
			box.h -= croppedHeight / 2;
		}
		cv::Rect objectRect(box.x, box.y, box.w, std::min<int>(box.w * 8, lastFrame.rows - box.y - 10));
		cv::Point objectPoint(box.x + box.w / 2, box.y + box.h / 2);
		cv::circle(lastFrame, objectPoint, 2, cv::Scalar(0, 255, 0), 2);
		//cv::rectangle(lastFrame, objectRect, cv::Scalar(0, 255, 0));

		centers.emplace_back(objectPoint);
		regions.push_back(objectRect);
	}
	
	cv::Mat grayFrame;
	cv::cvtColor(lastFrame, grayFrame, cv::COLOR_BGR2GRAY);
	tracker.Update(centers, regions, CTracker::RectsDist, grayFrame, correlation);
	for (auto & track : tracker.tracks)
	{
		cv::rectangle(lastFrame, track->lastRegion.m_rect, cv::Scalar(0, 0, 255));
		for (auto trace = track->trace.begin();
			(trace + 1) != track->trace.end() && trace != track->trace.end();
			trace++)
		{
			cv::line(lastFrame, *trace, *(trace + 1), track->color, 2);
		}
		if (!track->trace.empty())
		{
			cv::putText(lastFrame, track->name, track->trace.back(),
				cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 0, 0), 2);
		}
	}
	return tracker.tracks;
}

std::map<size_t, NamedFace>
	IntersectionDetector::correlateFacesWithObjects(const std::vector<bbox_t> & boxes,
													const std::vector<NamedFace> & faces)
{
	std::cerr << "Boxes: " + std::to_string(boxes.size())
		<< " Faces: " + std::to_string(faces.size()) << std::endl;
	std::map<size_t, NamedFace> correlation;
	for (auto face : faces)
	{
		cv::Point centerFace(face.face.x + face.face.width / 2, face.face.y + face.face.height / 2);
		std::cerr << "Face point: " << centerFace << std::endl;
		size_t minDistanceIndex = 0;
		double minDistance = -1;
		for (int i = 0; i < boxes.size(); i++)
		{
			cv::Point possibleFaceLocation(boxes[i].x + boxes[i].w / 2, boxes[i].y + boxes[i].h / 8);
			std::cerr << "Detect point: " << possibleFaceLocation << std::endl;
			if (!isPointInSegmentBox(Segment{cv::Point(boxes[i].x, boxes[i].y),
											cv::Point(boxes[i].x + boxes[i].w, boxes[i].y + boxes[i].h)},
									centerFace))
			{
				continue;
			}
			auto distance = distPointToPoint(centerFace, possibleFaceLocation);

			if (minDistance == -1 || distance < minDistance)
			{
				minDistance = distance;
				minDistanceIndex = i;
			}
		}
		if (minDistance != -1)
		{
			std::cerr << "Correlated to " + std::to_string(minDistanceIndex) << std::endl;
			correlation[minDistanceIndex] = face;
		}
	}
	std::cerr << "===================================================================================\n";
	return correlation;
}

std::vector<std::string> IntersectionDetector::getTrackNames() const
{
	std::vector<std::string> names;
	for (auto & track : tracker.tracks)
	{
		if (!track->name.empty())
		{
			std::cout << track->name << std::endl;
			names.push_back(track->name);
		}
	}
	return names;
}

void IntersectionDetector::joinOverlapped()
{
	auto detectIt = lastDetects.begin();
	while (detectIt != lastDetects.end())
	{
		int equals = 0;
		auto toRemove = std::remove_if(lastDetects.begin(), lastDetects.end(),
			[&detectIt, &equals] (bbox_t box)
		{
			if (box.x == detectIt->x &&
				box.y == detectIt->y &&
				box.w == detectIt->w &&
				box.h == detectIt->h)
			{
				if (equals++ > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}

			if (box.x >= detectIt->x &&
				box.y >= detectIt->y &&
				box.x + box.w <= detectIt->x + detectIt->w &&
				box.y + box.h <= detectIt->y + detectIt->h) //inside other detect
			{
				return true;
			}
			else if (abs(box.x - detectIt->x) + abs(box.w - detectIt->w + (box.x - detectIt->x)) < 20 &&
				abs(box.y - detectIt->y) + abs(box.h - detectIt->h + (box.y - detectIt->y)) < 20 &&
				box.w * box.h < detectIt->w * detectIt->h)
			{
				return true;
			}
			else
			{
				return false;
			}
		});

		if (toRemove != lastDetects.end())
		{
			lastDetects.erase(toRemove, lastDetects.end());
			detectIt = lastDetects.begin();
		}
		else
		{
			detectIt++;
		}
	}

	/*auto lastDetectsCopy = lastDetects;
	lastDetects.erase(std::remove_if(lastDetects.begin(), lastDetects.end(),
					[lastDetectsCopy] (bbox_t box)
					{
						for (auto detect : lastDetectsCopy)
						{
							if (box.x == detect.x &&
								box.y == detect.y &&
								box.w == detect.w &&
								box.h == detect.h)
							{
								continue;
							}

							if (box.x >= detect.x &&
								box.y >= detect.y &&
								box.x + box.w <= detect.x + detect.w &&
								box.y + box.h <= detect.y + detect.h) //inside other detect
							{
								return true;
							}
							else if (abs(box.x - detect.x) + abs(box.w - detect.w + (box.x - detect.x)) < 20 &&
								abs(box.y - detect.y) + abs(box.h - detect.h + (box.y - detect.y)) < 20 &&
								box.w * box.h < detect.w * detect.h)
							{
								return true;
							}
							else
							{
								return false;
							}
						}
					}),
		lastDetects.end());*/
}