#include <map>
#include <vector>
#include "opencv2/opencv.hpp"

#include "yolo_v2_class.hpp"
#include "Ctracker.h"
#include "Tuner.h"
#include "jsonIO.h"
#include "mymath.h"
#include "../Util.hpp"


class IntersectionDetector
{
public:
	enum Type{ Inside, Outside };
	struct Data
	{
		Type t;
		size_t polygonID;
		Intersection::Direction direction;
	};

	IntersectionDetector(const std::map<std::string,std::vector<cv::Point>>& polygons);

	std::vector<bbox_t> detect(cv::Mat frame, float thresh = 0.4);
	const tracks_t& detectIntersections(const std::vector<NamedFace> & faces);
	std::map<size_t, NamedFace>
		correlateFacesWithObjects(const std::vector<bbox_t> & boxes, const std::vector<NamedFace> & faces);

	std::vector<std::string> getTrackNames() const;

private:
	void joinOverlapped();

	CTracker tracker;
	Detector detector;
	std::vector<bbox_t> lastDetects;
	cv::Mat lastFrame;
};
