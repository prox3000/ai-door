#include "jsonIO.h"

CamPolygons readCamPoly(const std::string & filename, const std::string& camName)
{
	std::vector<cv::Point> polygon;
	std::vector<std::vector<cv::Point> > polygons;
	cv::Point point;
	std::string polyName;
	std::map<std::string, std::vector<cv::Point> > result;

	std::ifstream ifs(filename);
    Json::Reader reader;
    Json::Value obj;
    reader.parse(ifs, obj);
	
	Json::Value& a = obj[camName];
	//std::cout<<camName<<'\n';
		for (int i = 0; i < a.size(); ++i)
		{	
			polyName = a[i]["key"].asString();
			//std::cout<<a[i]["key"].asString()<<": \n";
			Json::Value& v	= a[i]["value"];
			for(int j = 0; j < v.size(); ++j)
			{
				polygon.push_back(cv::Point(v[j]["x"].asInt(), v[j]["y"].asInt()));
				//std::cout<<"x: "<<v[j]["x"].asInt()<< "y: "<<v[j]["y"].asInt()<<'\n';
			}
			result.insert(std::make_pair(polyName, polygon));
			polygon.clear();
		}
		std::map<std::string, std::vector<cv::Point> >::iterator pos;
		pos = result.find("resolution");
		if (pos != result.end()) 
		{
			point = pos->second[0];
			result.erase(pos);
		}

		CamPolygons camPolygons = { camName, cv::Size(point.x,point.y), result };

	return camPolygons;
}

std::vector<CamPolygons> readAll(const std::string & filename)
{
	std::vector<CamPolygons> result;
	std::vector<std::string> camList;

	//connect to file and read 
	std::ifstream ifs(filename);
    Json::Reader reader;
    Json::Value obj;
    reader.parse(ifs, obj);
	//saving readed from file
	Json::Value& arr = obj["camList"];
	//std::cout<<"readed: \n";
	for (int i = 0; i < arr.size(); ++i)
	{
		/*camList.push_back(arr[i].asString());
		const std::string str = camList[i];*/
		
		result.push_back(readCamPoly(filename,arr[i].asString()));
	}
	/*for (int i = 0;i < camList.size();++i)
	{
		const std::string str = camList[i];
		result.push_back(jsonReader::readCamPoly(filename,str));
	}*/

	return result;
}