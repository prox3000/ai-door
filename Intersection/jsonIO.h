#pragma once
#include <iostream>
#include <fstream>
#include <cstdio>
#include <vector>
#include <map>
#include <opencv2/core.hpp>
#include <jsoncpp/json/json.h>  // to build -ljsoncpp


struct CamPolygons 
{
	std::string camName;
	cv::Size resolution;
	std::map<std::string,std::vector<cv::Point>> polygons;
};

CamPolygons readCamPoly(const std::string& filename, const std::string& camName);
std::vector<CamPolygons> readAll(const std::string & filename);