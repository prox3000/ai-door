#pragma once
#include <iostream>
#include <vector>
#include <memory>
#include <array>

#include "opencv2/opencv.hpp"

#include "defines.h"
#include "Kalman.h"
#include "mymath.h"
#include "intersection.h"
#include "DBAccessor.hpp"
#include "../Util.hpp"

// --------------------------------------------------------------------------
class CTrack
{
public:
	void setPolygons(const std::map<std::string, std::vector<cv::Point>>& p)
	{
		idata.polygons = p;
		idata.oldPoints.resize(p.size(), cv::Point(-1, -1));
		idata.newPoint = cv::Point(-1, -1);
	}

    CTrack(
            const Point_t& p,
            const CRegion& region,
            track_t dt,
            track_t Accel_noise_mag,
            size_t trackID,
			const std::map<std::string, std::vector<cv::Point>>& polygons,
			const std::string & _name
            )
		:
		track_id(trackID),
		skipped_frames(0),
        lastRegion(region),
        pointsCount(0),
		prediction(p),
		KF(p, dt, Accel_noise_mag),
		name(_name),
		nameCandidate(_name)
	{
		std::cerr << "Creating new track\n";
		setPolygons(polygons);
		idata.name = &name;

		srand(time(0));
		color = cv::Scalar(rand() % 256,rand() % 256,rand() % 256);
	}

	track_t CalcDist(const Point_t& p)
	{
		Point_t diff = prediction - p;
		return sqrtf(diff.x * diff.x + diff.y * diff.y);
	}

	track_t CalcDist(const cv::Rect& r)
	{
		std::array<track_t, 4> diff;
        diff[0] = prediction.x - lastRegion.m_rect.width / 2 - r.x;
        diff[1] = prediction.y - lastRegion.m_rect.height / 2 - r.y;
        diff[2] = static_cast<track_t>(lastRegion.m_rect.width - r.width);
        diff[3] = static_cast<track_t>(lastRegion.m_rect.height - r.height);

		track_t dist = 0;
		for (size_t i = 0; i < diff.size(); ++i)
		{
			dist += diff[i] * diff[i];
		}
		return sqrtf(dist);
	}

    void Update(const Point_t& p, const CRegion& region, bool dataCorrect, size_t max_trace_length, const std::string & _name)
	{
		lastFrameTime = time(0);
		if (!_name.empty())
		{
			if (name == nameCandidate && _name != name)
			{
				nameCandidate = _name;
			}
			else if (name != nameCandidate && _name == nameCandidate)
			{
				name = nameCandidate;
			}
			else if (name != nameCandidate && _name != nameCandidate)
			{
				nameCandidate = _name;
			}
		}
		std::cerr << "Name: " + name + " - Name candidate: " + nameCandidate << std::endl;

		KF.GetPrediction();

        if (pointsCount)
        {
            if (dataCorrect)
            {
                prediction = KF.Update((p + averagePoint) / 2, dataCorrect);
                //prediction = (prediction + averagePoint) / 2;
            }
            else
            {
                prediction = KF.Update(averagePoint, dataCorrect);
            }
        }
        else
        {
            prediction = KF.Update(p, dataCorrect);
        }

		if (dataCorrect)
		{
			auto smoothedHeight = region.m_rect.height + (lastRegion.m_rect.height - region.m_rect.height) / 4;
			auto reg = region;
			reg.m_rect.height = smoothedHeight;
			lastRegion = reg;

			////////detect intersections and update database
			auto newIntersections = detectIntersection(idata,
				cv::Point(region.m_rect.x + region.m_rect.width / 2,
					region.m_rect.y + region.m_rect.height));
			std::cerr << "New intersections size: " + std::to_string(newIntersections.size()) << std::endl;
			//detectIntersection(idata, region);
			intersections.insert(intersections.end(), newIntersections.begin(), newIntersections.end());
		}

		if (trace.size() > max_trace_length)
		{
			trace.erase(trace.begin(), trace.end() - max_trace_length);
		}

		trace.push_back(prediction);
	}

	cv::Scalar color;
	std::vector<Intersection> intersections;
	time_t lastFrameTime = time(0);
	std::string name;
	std::string nameCandidate;
	std::vector<Point_t> trace;
	size_t track_id;
	size_t skipped_frames;
    CRegion lastRegion;
    int pointsCount;
    Point_t averagePoint;

	cv::Rect GetLastRect()
	{
		return cv::Rect(
            static_cast<int>(prediction.x - lastRegion.m_rect.width / 2),
            static_cast<int>(prediction.y - lastRegion.m_rect.height / 2),
            lastRegion.m_rect.width,
            lastRegion.m_rect.height);
	}

	bool hasName() const { return name != "UNRECOGNIZED" && !name.empty(); }

private:
	Point_t prediction;
	TKalmanFilter KF;

	IntersectionData idata;
};

typedef std::vector<std::unique_ptr<CTrack>> tracks_t;
