#include <algorithm>
#include <iterator>
#include <stack>

#include "IntersectionAnalyzer.h"


bool isStanding(const std::unique_ptr<CTrack> & track)
{
	if (track->name == "UNRECOGNIZED" || track->name.empty())
	{
		return false;
	}

	bool moving = true;
	if (track->trace.size() > 1)
	{
		auto it = track->trace.rbegin();
		auto minX = it->x;
		auto maxX = it->x;
		auto lastFrameTime = it->t;
		int timeDelta = 0; //in seconds
		while (it != track->trace.rend() &&
			(maxX - minX) < DELTA_BETWEEN_ALL_FRAMES)
		{
			timeDelta = lastFrameTime - it->t;
			if (timeDelta >= 1)
			{
				moving = false;
				break;
			}

			it++;
			minX = std::min(minX, it->x);
			maxX = std::max(maxX, it->x);
		}
	}
	return !moving;
}


IntersectionAnalyzer::IntersectionAnalyzer(const std::map<std::string, std::vector<IntersectionRecord>>& e,
	std::function<bool(const std::unique_ptr<CTrack> & track)> f)
	: events(e), isCloseToDoor(f)
{
	dba = std::make_unique<DBAccessor>(cfg);
	if (!dba->isConnected())
	{
		std::cout << "COULD NOT CONNECT TO DATABASE\n";
	}
	else
	{
		std::cout << "Connected\n";
	}
}

IntersectionAnalyzer IntersectionAnalyzer::load(const std::string& location)
{
	using namespace std;
	const std::string eventsFilename = "data/intersection.cfg";
	map<string, vector<IntersectionRecord>> events;
	vector<string> eventNames = ReadHeader(eventsFilename.c_str());
	for (auto eventName : eventNames)
	{
		auto eventVector = getParams(eventsFilename, eventName);
		vector<IntersectionRecord> polygons;

		transform(eventVector.begin(), eventVector.end(),
			back_inserter<vector<IntersectionRecord>>(polygons),
			[](pair<string, string> p) -> IntersectionRecord
			{
				auto key = p.first;
				return make_pair<string, Intersection::Direction>(move(key),
					(p.second == "Left" ?
					Intersection::Direction::Left :
					Intersection::Direction::Right));
			});

		events[eventName] = polygons;
	}

	std::function<bool(const std::unique_ptr<CTrack> & track)> isCloseToDoor;
	if (location == "Outside")
	{
		isCloseToDoor = isStanding;
	}
	else if (location == "Inside")
	{
		IntersectionRecord ir = std::make_pair<std::string, Intersection::Direction>("vertical", Intersection::Direction::Right);
		isCloseToDoor = [ir](const std::unique_ptr<CTrack> & track) -> bool
		{
			auto itGood = std::find_if(track->intersections.begin(), track->intersections.end(),
				[ir](Intersection is)
			{
				return is.polygonID == ir.first && is.direction == ir.second;
			});
			if (itGood == track->intersections.end())
			{
				return false;
			}

			auto itBad = std::find_if(itGood, track->intersections.end(),
				[ir](Intersection is)
			{
				return is.polygonID == ir.first && is.direction != ir.second;
			});
			return itBad == track->intersections.end();
		};
	}
	if (isCloseToDoor)
	{
		std::cout << "Good target\n";
	}
	else
	{
		std::cout << "Bad target\n";
	}
	return IntersectionAnalyzer(events, isCloseToDoor);
}

bool IntersectionAnalyzer::detectInOut(const tracks_t& tracks, const tracks_t& deadTracks)
{
	bool need_open_door=false;
	using namespace std;
	if (events.empty())
	{
		return false;
	}

	for (auto & track : tracks)
	{
		if (!need_open_door)
		{
			need_open_door = isCloseToDoor(track);
			if (need_open_door)
			{
				log(track->name + " is close to door");
			}
		}
		std::cerr << "Checking events for " + track->name << std::endl;
		for (auto event : events)
		{
			/*for (auto poly : event.second)
			{
				std::cout << poly.first << std::endl;
			}*/
			stack<IntersectionRecord> records;
			auto itNextPoly = event.second.begin();
			bool eventDone = false;
			for (auto intersection : track->intersections)
			{
				//std::cout << intersection.polygonID << std::endl;
				if (!track->hasName())
				{
					std::cerr << ",";
					continue;
				}
				auto itRecord = find_if(event.second.begin(), event.second.end(),
					[intersection](IntersectionRecord record) //find if intersection is in list
					{
						return record.first == intersection.polygonID;
					});
				if (itRecord == event.second.end())
				{
					std::cerr << ".";
					continue;
				}

				std::cerr << "=";
				if (itRecord == itNextPoly &&
					intersection.direction == itRecord->second)
				{
					std::cerr << "One intersection of event: " << event.first << std::endl;
					records.push(*itRecord);
					itNextPoly++;
					if (itNextPoly == event.second.end())
					{
						writeEvent(track->name, event.first);
						eventDone = true;
						log("Event '" + event.first + "' done by " + track->name);

						/*else if (track->skipped_frames == skipFrames)
						{
							writeUnknown(event.first);
							eventDone = true;
							log("write unknown");
						}*/

						break;
					}
				}

				if (std::find(event.second.begin(), itNextPoly, *itRecord) != itNextPoly)
				{
					while (records.top() != *itRecord)
					{
						records.pop();
					}
					itNextPoly = itRecord + 1;
					if (intersection.direction != itRecord->second)
					{
						records.pop();
						itNextPoly = itRecord;
					}
				}
			}
			if (eventDone)
			{
				track->intersections.clear();
			}
		}
		std::cerr << "Events checked for this track\n";
	}

	/*for (auto & track : deadTracks)
	{
		for (auto event : events)
		{
			stack<IntersectionRecord> records;
			auto itNextPoly = event.second.begin();
			bool eventDone = false;
			for (auto intersection : track->intersections)
			{
				//TODO implement
			}
		}
	}*/

	return need_open_door;
}

void IntersectionAnalyzer::detectEventByFaceSize(const std::vector<NamedFace>& faces,
	cv::Size minFaceSize, const std::string& event)
{
	std::vector<std::string> newBigFaces;
	for (auto face : faces)
	{
		if (face.face.area() < minFaceSize.area())
		{
			std::cout << "Area of face is small\n"
				<< "Min face size: " << minFaceSize << std::endl
				<< "Face size: " << face.face << std::endl;
			continue;
		}
		if (face.name_face == "UNRECOGNIZED" ||
			face.face.area() < minFaceSize.area())
		{
			continue;
		}

		newBigFaces.push_back(face.name_face);
	}

	for (auto newBigFace : newBigFaces)
	{
		auto faceIt = bigFaces.find(newBigFace);
		if (faceIt != bigFaces.end())
		{
			faceIt->second.appeared++;
			std::cout << faceIt->second.appeared << std::endl;
		}
		else
		{
			bigFaces.insert(std::pair<std::string, BigFace>(newBigFace, { 1, time(0) }));
			std::cout << 1 << std::endl;
		}
	}
	auto now = time(0);
	for (auto it = bigFaces.begin(); it != bigFaces.end(); )
	{
		if (now - it->second.time > 5)
		{
			it = bigFaces.erase(it);
		}
		else if (it->second.appeared == 3)
		{
			if (dba->getLastEventByName(it->first) != event)
			{
				log("Event '" + event + "' done by BIG FACE: " + it->first);
				writeEvent(it->first, event);
			}
			it = bigFaces.erase(it);
		}
		else
		{
			++it;
		}
	}
}

void IntersectionAnalyzer::writeEvent(const std::string& trackName, const std::string& eventName)
{
	std::string query = "INSERT INTO Entry(name, event) VALUES(";
	query += "\"" + (trackName.empty() ? std::string("-") : trackName) + "\", ";
	query += "\"" + eventName + "\"";
	query += ");";
	std::cout << query << std::endl;
	DBAccessor db(cfg);
	db.execDirect(query);
	//dba->execDirect(query);
	log("Inserted");
}

void IntersectionAnalyzer::writeUnknown(const std::string& eventName)
{
	std::string query = "INSERT INTO Event(name, event) VALUES(";
	query += "\"UNRECOGNIZED\", ";
	query += "\"" + eventName + "\"";
	query += ");";
	std::cout << query << std::endl;
	dba->execDirect(query);
}