#pragma once
#include <vector>
#include <memory>
#include <functional>

#include "Intersection/intersection.h"
#include "Intersection/DBAccessor.hpp"
#include "Intersection/track.h"
#include "Intersection/Tuner.h"
#include "Util.hpp"

const int DELTA_BETWEEN_TWO_FRAMES = 50;
const int DELTA_BETWEEN_ALL_FRAMES = 120;

bool isStanding(const std::unique_ptr<CTrack> & track);

class IntersectionAnalyzer
{
	using IntersectionRecord = std::pair<std::string, Intersection::Direction>;
public:
	IntersectionAnalyzer(const std::map<std::string, std::vector<IntersectionRecord>>& e,
		std::function<bool(const std::unique_ptr<CTrack> & track)> f);
	static IntersectionAnalyzer load(const std::string& location);

	bool detectInOut(const tracks_t& tracks, const tracks_t& deadTracks = tracks_t());
	void detectEventByFaceSize(const std::vector<NamedFace>& faces,
		cv::Size minFaceSize, const std::string& event);

private:
	//bool detectInOut(const std::unique_ptr<CTrack>& track);

	std::function<bool(const std::unique_ptr<CTrack> & track)> isCloseToDoor;
	void writeEvent(const std::string& trackName, const std::string& eventName);
	void writeUnknown(const std::string& eventName);

	std::map<std::string, std::vector<IntersectionRecord>> events;

	struct BigFace
	{
		int appeared;
		time_t time;
	};
	std::map<std::string, BigFace> bigFaces;
	
    DBAccessor::Config cfg = DBAccessor::Config{ "localhost","root","root","intersection" };
    std::unique_ptr<DBAccessor> dba;
};