all:
	g++ -pthread -std=c++14 -o a *.cpp Intersection/*.cpp `pkg-config opencv --cflags --libs` -ljsoncpp -lcpprest -lboost_system -lssl -lcrypto -I/usr/include/mysql \
	-L/usr/lib/x86_64-linux-gnu -lmysqlclient -lpthread ~/darknet/darknet-master/darknet.so -DOPENCV=1
