#include "Util.hpp"

#include <regex>

#include "Intersection//DBAccessor.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

#include <time.h>
#include <chrono>
#include <mutex>

std::mutex logMutex;

void log(const std::string & msg)
{
	using namespace std::chrono;
	std::lock_guard<std::mutex> lock(logMutex);
	system_clock::time_point p = system_clock::now();
	std::time_t t = system_clock::to_time_t(p);
	std::clog << std::ctime(&t) << ": " << msg << std::endl;
}

using namespace std;

int counter_temp = 0;
int state_door=0;
bool isOpenPort = false;
bool isOpenDoor=false; 

/* Напоминалка о структуре
struct tm {
         tm_sec,                         // секунды 0..59 
         tm_min,                         // минуты  0..59 
         tm_hour,                        // час дня 0..23 
         tm_mday,                    // день месяца 1..31 
         tm_mon,                           // месяц 0..11 
         tm_year,                       // год после 1900 
         tm_wday,         // день недели, 0..6 (Sun..Sat) 
         tm_yday,                    // день года, 0..365 
         tm_isdst;        // >0, если есть поправка на сдвиг,
                          //   =0, если нет поправки,
                          //   <0, если неизвестно 
};
*/

void Config_params::parseParams(const std::string & camNumber)
{

	try
	{
		camID = getParams(CAM_CONFIG_NAME, camNumber, { "cam" }).at("cam");
	}
	catch (const std::out_of_range & ex)
	{
		std::cout << ex.what() << std::endl;
		return;
	}
	try
	{
		cam_url = getParams(CAM_CONFIG_NAME, "List", { camID }).at(camID);
	}
	catch (const std::out_of_range & ex)
	{
		std::cout << ex.what() << std::endl;
		return;
	}
	auto params = getParams(CAM_CONFIG_NAME, camNumber,
		{ "sens", "prewrite", "postwrite","x", "y", "w", "h", "thresh","sizeface","openD","location", "event"});

	try
	{
		sens = stoi(params.at("sens"));
		prewrite = stoi(params.at("prewrite"));
		postwrite = stoi(params.at("postwrite"));
		thresh = stof(params.at("thresh"));
		roi.x = stoi(params.at("x"));
		roi.y = stoi(params.at("y"));
		roi.height = stoi(params.at("h"));
		roi.width = stoi(params.at("w"));
		size_face=stoi(params.at("sizeface"));
		odoor=stoi(params.at("openD"));
		location=params.at("location");
		event = params.at("event");
	}
	catch (const std::out_of_range & ex)
	{
		std::cout << ex.what() << std::endl;
		return;
	}
	catch (const std::exception & ex)
	{
		std::cout << "I am here" << std::endl;
		std::cout << ex.what() << std::endl;
		return;
	}
		
}

void Config_params_all::parse_params()
{
	try
	{
		schedule.start = getParams(CAM_CONFIG_NAME, "Schedule", { "start" }).at("start");
		schedule.end = getParams(CAM_CONFIG_NAME, "Schedule", { "end" }).at("end");
		std::cout<<"Schedule "<<schedule.start<<"-"<<schedule.end<<endl;
	
		door.ip=getParams(CAM_CONFIG_NAME, "Setting", { "doorip" }).at("doorip");
		door.port=getParams(CAM_CONFIG_NAME, "Setting", { "doorport" }).at("doorport");
		std::cout<<"door ip "<<door.ip<<"  door port "<<door.port<<endl;
		
		recog.ip=getParams(CAM_CONFIG_NAME, "Setting", { "recogip" }).at("recogip");
		recog.port=getParams(CAM_CONFIG_NAME, "Setting", { "recogport" }).at("recogport");
		std::cout<<"recog ip "<<recog.ip<<"  recog port "<<recog.port<<endl;
	
	}
	catch (const std::out_of_range & ex)
	{
		std::cout << ex.what() << std::endl;
		return;
	}	
		
}

std::map<std::string, std::string>
getParams(const std::string & filename,
	const std::string & field,
	const std::vector<std::string> & keys)
{
	std::map<std::string, std::string> params;
	std::ifstream inf(filename);
	if (!inf.is_open())
	{
		return params;
	}
	
	std::string line;
	std::getline(inf, line);
	
	while (line != std::string("[" + field + "]") && inf)
	{
		std::getline(inf, line);
	}
	vector<string> configStrings;
	std::getline(inf, line);
	while (!std::regex_search(line, std::regex(R"(^\[.*\]$)")) && inf)
	{
		configStrings.push_back(line);
		std::getline(inf, line);
	}
	std::for_each(keys.begin(), keys.end(), [&params, configStrings](std::string key)
	{
		for (auto str : configStrings)
		{
			if (str.substr(0, str.find('=')) == key)
			{
				params[key] = str.substr(str.find('=') + 1);
				break;
			}
		}
	});
	return params;
}

std::vector<std::pair<std::string, std::string>>
getParams(const std::string & filename,
	const std::string & field)
{
	std::vector<std::pair<std::string, std::string>> params;
	std::ifstream inf(filename);
	if (!inf.is_open())
	{
		return params;
	}
	
	std::string line;
	std::getline(inf, line);
	
	while (line != std::string("[" + field + "]") && inf)
	{
		std::getline(inf, line);
	}
	vector<string> configStrings;
	std::getline(inf, line);
	while (!std::regex_search(line, std::regex(R"(^\[.*\]$)")) && inf)
	{
		configStrings.push_back(line);
		std::getline(inf, line);
	}
	for (auto str : configStrings)
	{
		if (str.find('=') == std::string::npos)
		{
			continue;
		}
		std::string key = str.substr(0, str.find('='));
		params.emplace_back(key, str.substr(str.find('=') + 1));
	}
	return params;
}

cv::Point shiftAndScale(const cv::Point point, cv::Point shift, float scaleFactor)
{
	cv::Point res(point);
	res += shift;
	res *= scaleFactor;
	return res;
}

cv::Rect shiftAndScale(const cv::Rect rect, cv::Point shift, float scaleFactor)
{
	cv::Rect res(rect);
	res += shift;
	res.x *= scaleFactor;
	res.y *= scaleFactor;
	res.width *= scaleFactor;
	res.height *= scaleFactor;
	return res;
}

cv::Size scale(const cv::Size size, float scaleFactor)
{
	auto res = size;
	res.width *= scaleFactor;
	res.height *= scaleFactor;
	return res;
}

cv::Rect adjustROI(cv::Rect rect, cv::Size size)
{
	auto res = rect;
	res.x = std::max(res.x, 0);
	res.y = std::max(res.y, 0);
	res.x = std::min(res.x, size.width);
	res.y = std::min(res.y, size.height);
	res.width = std::min(size.width - res.x, res.width);
	res.height = std::min(size.height - res.y, res.height);
	return res;
}

std::string SaveImg(const cv::Mat& image, bool isRecognized)
{
	char save_time[30], file_name[30];
	time_t now = time(NULL);
	
	strftime(save_time, 20, "%Y-%m-%d_%H-%M-%S", localtime(&now));
	sprintf(file_name, "%s_%d", save_time, counter_temp);
	counter_temp++;
	if(counter_temp>5)counter_temp=0;
	std::string image_path = isRecognized ? "Image_Recognized//" : "Image_Not_Recognized//", directory_name = ".jpg";
	image_path += file_name + directory_name;
	imwrite(image_path, image);
	return image_path;
}

void SaveImg(const cv::Mat& image, bool isRecognized, std::string name)
{
	//std::cout << "Name: " << name << std::endl;
	char save_time[30], file_name[30];
	time_t now = time(NULL);
	
	strftime(save_time, 20, "%Y-%m-%d_%H-%M-%S", localtime(&now));
	sprintf(file_name, "%s_%d", save_time, counter_temp);
	counter_temp++;
	if(counter_temp>5)counter_temp=0;
	std::string image_path = isRecognized ? "Image_Recognized//" : "Image_Not_Recognized//", directory_name = ".jpg";
	image_path += file_name + name+ directory_name;
	std::cout<<"Saved as "<<image_path<<std::endl;
	imwrite(image_path, image);
}

int msleep(unsigned long milisec)  
{  
 
    struct timespec req={0};  
 
    time_t sec=(int)(milisec/1000);  
 
    milisec=milisec-(sec*1000);  
 
    req.tv_sec=sec;  
 
    req.tv_nsec=milisec*1000000L;  
 
    while(nanosleep(&req,&req)==-1)  
 
        continue;  
 
    return 1;  
 
}  

bool insert(int doornum, std::string input)
{
 DBAccessor::Config cfg = { "localhost","root","root","intersection" };
 DBAccessor db(cfg); 
 std::string s = "INSERT INTO door(doornum, status) VALUES(";
 s += std::to_string(doornum);
 s += ", \"";
 s += input + "\");";

 return db.execDirect(s);
 }

bool insert_come(int polynum, std::string dir, std::string name, std::string cam_id )
{
	std::cout<<"Qwerty ";
	std::string quote="\",\"";
 DBAccessor::Config cfg = { "localhost","root","root","intersection" };
 DBAccessor db(cfg);  
	std::string s = "INSERT INTO counter(polynum, dir, name, cam_id) VALUES(";
	s += std::to_string(polynum);
	s += ", \"";
	s += dir;
	s += quote;
	s += name;
	s += quote;
	s += cam_id;
	s += "\");"; 
	std::cout<<s<<std::endl;
	
	return db.execDirect(s);
}

const vector<string> explode(const string& s, const char& c)
{
	string buff{""};
	vector<string> v;
	
	for(auto n:s)
	{
		if(n != c) buff+=n; else
		if(n == c && buff != "") { v.push_back(buff); buff = ""; }
	}
	if(buff != "") v.push_back(buff);
	
	return v;
}

bool true_period(Config_params_all params_all)
{
	
		struct tm *t;
        time_t ltime;
        time(&ltime);
        t = localtime(&ltime);
		
		vector<string> v1{explode(params_all.schedule.start, ':')};
		
		vector<string> v2{explode(params_all.schedule.end, ':')};
	
		int schedule_in=0,schedule_out=0;
		int diff_hour1 = t->tm_hour-stoi(v1[0]);
		
		if(diff_hour1==0)
		{
			if((t->tm_min-stoi(v1[1]))>=0)
			schedule_in=1;
			else
			schedule_in=0;
		}
		else if(diff_hour1>0)schedule_in=1;
		
		int diff_hour2 = stoi(v2[0])-t->tm_hour;
		
		if(diff_hour2==0)
		{
			if((stoi(v2[1])-t->tm_min)>=0)
			schedule_out=1;
			else
			schedule_out=0;
		}
		else if(diff_hour2>0)schedule_out=1;
		
		
		if(schedule_in&&schedule_out)
		return true;
		else
		return false;
	
}

void error(const char *msg)
{
    perror(msg);
}

int OpenDoor(int argc,const char *argv[])
{
	
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    }
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
	
	int enable = 1;
	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
    error("setsockopt(SO_REUSEADDR) failed");
	
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("ERROR connecting");
    n = write(sockfd,"open",4);
    if (n < 0) 
         error("ERROR writing to socket");
    close(sockfd);
    return 0;
}

int StatusDoor(int argc,const char *argv[])
{
	
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
	char buffer[256];
	if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    }
	int prevstatedoor=0;
	while(1)
	{
        
		portno = atoi(argv[2]);
		sockfd = socket(AF_INET, SOCK_STREAM, 0);
		if (sockfd < 0) 
			error("ERROR opening socket");
		
		int enable = 1;
		if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
		error("setsockopt(SO_REUSEADDR) failed");		
		
		server = gethostbyname(argv[1]);
		if (server == NULL) {
			fprintf(stderr,"ERROR, no such host\n");
			exit(0);
		}
		bzero((char *) &serv_addr, sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		bcopy((char *)server->h_addr, 
			(char *)&serv_addr.sin_addr.s_addr,
			server->h_length);
		serv_addr.sin_port = htons(portno);
		if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
			error("ERROR connecting");
//		printf("send message: ");
		bzero(buffer,256);
		sprintf(buffer,"%s","state");
		strtok(buffer, "\n");
		n = write(sockfd,buffer,strlen(buffer));
		if (n < 0) 
			error("ERROR writing to socket");
		bzero(buffer,256);
		n = read(sockfd,buffer,255);
		if (n < 0) 
			error("ERROR reading from socket");
		//printf("%s_",buffer);
		int temp=stoi(buffer);
		
		if(temp!=prevstatedoor)
		{	
		//std::cout<<"prevstatedoor "<<prevstatedoor;
			if(prevstatedoor>0)
			{
				if(temp==1)
				{
					insert(1,"closed");
					state_door=1;
				}
				else if(temp==2)
				{
					insert(1,"open");	
					state_door=2;
				}
				else
				{
					insert(1,"uknown");	
					state_door=0;
				}
			}
			prevstatedoor=temp;
			std::cout<<" >> "<<prevstatedoor<<std::endl;
		
		}
		//printf("%i",prevstatedoor);
		
		close(sockfd);
		msleep(1000);
	}
    return 0;
}

std::string SendFace(int argc,const char *argv[])
{
	const int BUFF_SIZE = 256;
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    char buffer[BUFF_SIZE] = { '\0' };
    /*if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    }*/
    portno = atoi(argv[2]);
	
	auto lambdaDeleter = [] (int* pInt)
	{
		close(*pInt);
	};
	std::unique_ptr<int, decltype(lambdaDeleter)> socketGuard(&sockfd, lambdaDeleter);
	try
	{
		sockfd = socket(AF_INET, SOCK_STREAM, 0);
		//std::cout<<"connect to socket"<<buffer<<std::endl;
		if (sockfd < 0)
		{
			error("ERROR opening socket\n");
			bzero(buffer, BUFF_SIZE);
			return buffer;
		}
		server = gethostbyname(argv[1]);
		if (server == NULL) {
			fprintf(stderr, "ERROR, no such host\n");
			bzero(buffer, BUFF_SIZE);
			return buffer;
		}
		bzero((char *) &serv_addr, sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		bcopy((char *)server->h_addr, 
				(char *)&serv_addr.sin_addr.s_addr,
				server->h_length);
		serv_addr.sin_port = htons(portno);
		if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
		{
			error("ERROR connecting");
			bzero(buffer, BUFF_SIZE);
			return buffer;
		}

		FILE *picture;
		int size, read_size, stat, packet_index=1;
		char send_buffer[10240], read_buffer[256];
		//std::cout<<"Ok,next open picture"<<buffer<<std::endl;
		picture = fopen(argv[3], "r"); 

		if (picture == NULL)
		{
			printf("Error Opening Image File\n");
			bzero(buffer, BUFF_SIZE);
			return buffer;
		} 

		fseek(picture, 0, SEEK_END);
		size = ftell(picture);
		fseek(picture, 0, SEEK_SET);

		//int32_t conv = htonl(size);
		//char *intdata = (char*)&conv;
		//n = write(sockfd,intdata,sizeof(conv));
		
		
		//std::cout<<"Ok,next we have khown size"<<buffer<<std::endl;
		bzero(buffer, BUFF_SIZE);
		sprintf(buffer, "%05i", size);	
		//std::cout<<"size image:"<<buffer<<std::endl;
		n = write(sockfd, buffer, strlen(buffer));
		
		if (n < 0) 
		{
			error("ERROR writing to socket");
			bzero(buffer, BUFF_SIZE);
			return buffer;
		}

		while(!feof(picture)) {
			//Read from the file into our send buffer
			read_size = fread(send_buffer, 1, sizeof(send_buffer)-1, picture);
		
			//Send data through our socket 
			do{
				stat = write(sockfd, send_buffer, read_size);
			} while (stat < 0);
		 
			packet_index++;  
			//Zero out our send buffer
			bzero(send_buffer, sizeof(send_buffer));
		}
			
		
		bzero(buffer, BUFF_SIZE);
		n = read(sockfd, buffer, 255);//TODO: replace 255 with BUFF_SIZE
		if (n < 0)
		{
			error("ERROR reading from socket");
			bzero(buffer, BUFF_SIZE);
			return buffer;
		}
		//close(sockfd);//TODO: move it to the end or better make a guard for it
	}
	catch(...)
	{
		std::cout<<"Error connection to recognize_server"<<std::endl;
		bzero(buffer, BUFF_SIZE);
	}
    
    return buffer;
}

std::string SendImg(const cv::Mat & image,Config_params_all params_all)
{
	std::string name_image="",who="";	
	//std::cout<<"Try to save"<<std::endl;
	name_image=SaveImg(image, false);
	//std::cout<<"Try to send"<<std::endl;
	const char *b[]={"b",params_all.recog.ip.c_str(),params_all.recog.port.c_str(),name_image.c_str()};
	who=SendFace(4,b);
	std::cout<<"Recognized as: "<<who<<std::endl;
	
	return 	who;
}

char* wtoc(const wchar_t* w, size_t max)
{
	char* c = new char[max];
	wcstombs(c, w, max);
	return c;
}

std::vector<std::string> ReadHeader(const char *input_file_name)
{
	ifstream in(input_file_name);
	std::string line;
	std::vector<std::string> section;
	while (getline(in, line))
	{
		std::istringstream iss(line);
		if (iss.get() == 91)
		{
			line=line.substr(1, line.size() - 2);
			section.push_back(line);
		}
	}
	in.close();
	return section;
}

void redirect_cout()
{
	freopen( "cout.html", "w", stdout );
}

void redirect_cerr()
{
	const int bufSize = 30;
	char buff[bufSize];
	auto t = time(0);
	strftime(buff, bufSize, "%Y-%m-%d_%H-%M-%Serror.html", localtime(&t));
	freopen( buff, "w", stderr );
	cerr << "Error message" << endl;
}

std::vector<cv::Rect>  BoxToRect(std::vector<bbox_t> v_input)
{
	std::vector<cv::Rect> v_output;	
	for (auto input : v_input)
	{
		cv::Rect objectRect(input.x, input.y, input.w, input.h);
		v_output.push_back(objectRect);		
	}	
	return v_output;
}

void AddBorder(cv::Mat frame,cv::Rect & face,int width,int height)
{
	face.x = std::max(face.x-width, 0);
	face.width = std::min(frame.cols - face.x, face.width+(width * 2));
	face.y = std::max(face.y-height, 0);
	face.height = std::min(frame.rows - face.y, face.height+(height * 2));
}

void AddBorder_without_removing(cv::Mat frame,cv::Rect & face,int x,int y)
{
	face.width=std::min(face.width+x,frame.cols - face.x);
	face.height=std::min(face.height+y,frame.rows - face.y);
}