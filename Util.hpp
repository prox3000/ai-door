#pragma once
#include "opencv2/opencv.hpp"
#include <chrono>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "Intersection/yolo_v2_class.hpp"

const char * const CAM_CONFIG_NAME = "config.cfg";

const std::vector<std::string> unrecognize={"incorr_sz","bigger","some","NO_FACE","UNRECOGNIZED","Incorr_img","Incorr_img1","Incorr_img2","BLURRED","NO_ENCODINGS"};

struct NamedFace {
	time_t time_detect;
	cv::Mat  img;
	cv::Mat  img_face;
	cv::Rect face;
	std::string name_face;
};


class Config_params
{
public:

	int sens,postwrite,prewrite;
	float thresh;
	std::string cam_url;
	cv::Rect roi;
	std::string camID;
	int size_face=150;
	int odoor = 0;
	std::string location;
	std::string direction;
	int polygonID;
	std::string event;
	
	void parse_params(std::wstring cam_number);
	void parseParams(const std::string & camNumber);

};

struct Schedule
{
	std::string start;	
	std::string end;
};

struct Address
{
	std::string ip;	
	std::string port;
};

class Config_params_all
{
public:
	Schedule schedule;	
	Address door;
	Address recog;
	void parse_params();
};

class LogStreamGuard
{
public:
	LogStreamGuard(const std::string & filename, std::ios_base::openmode m) : f(filename)
	{
		buf = std::clog.rdbuf(f.rdbuf());
	}
	~LogStreamGuard()
	{
		std::clog.rdbuf(buf);
		f.close();
	}

	void check() const
	{
		if (!f.is_open())
		{
			std::cerr << "Problem opening log file.\n"
				<< "No logging during this run.\n";
		}
		else
		{
			std::cout << "Log file opened.\n";
		}
	}

private:
	std::ofstream f;
	std::streambuf * buf;
};

void log(const std::string & msg);

std::map<std::string, std::string>
getParams(const std::string & filename,
	const std::string & field,
	const std::vector<std::string> & keys);
std::vector<std::pair<std::string, std::string>>
getParams(const std::string & filename,
	const std::string & field);

cv::Point shiftAndScale(const cv::Point point, cv::Point shift, float scaleFactor);
cv::Rect shiftAndScale(const cv::Rect rect, cv::Point shift, float scaleFactor);
cv::Size scale(const cv::Size size, float scaleFactor);
cv::Rect adjustROI(cv::Rect rect, cv::Size size);

std::string SaveImg(const cv::Mat & image, bool isRecognized);
void SaveImg(const cv::Mat& image, bool isRecognized, std::string name);
std::string SendImg(const cv::Mat & image,Config_params_all params_all);
std::string SendFace(int argc,const char *argv[]);
void OpenDoor();
int StatusDoor(int argc,const char *argv[]);
int OpenDoor(int argc,const char *argv[]);

int msleep(unsigned long milisec);
bool insert(int doornum, std::string status);
bool insert_come(int polynum, std::string dir, std::string name, std::string cam_id );
std::vector<std::string> ReadHeader(const char *input_file_name);
bool true_period(Config_params_all params_all);
void redirect_cout();
void redirect_cerr();
std::vector<cv::Rect>  BoxToRect(std::vector<bbox_t> v_input);
void AddBorder(cv::Mat frame,cv::Rect & face,int width,int height);
void AddBorder_without_removing(cv::Mat frame,cv::Rect & face,int x,int y);

template <typename T>
void any_clear(T &val)
{
	T tmp;
	std::swap(tmp, val);
}