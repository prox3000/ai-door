#include <iostream>
#include "Camera.h"
#include "Util.hpp"
#include "FrameQueue.h"

using namespace std;

int main()
{
	//redirect_cout();
	redirect_cerr();
	
	LogStreamGuard fGuard("log.html", std::ios_base::out);
	fGuard.check();
	
	std::unique_ptr<std::thread> p_state_door;
	
	Config_params_all params_all;
	params_all.parse_params();
	
	Config_params config_for_cam0;
	config_for_cam0.parseParams("0");
	Camera camera0(config_for_cam0,params_all);
	
	Config_params config_for_cam1;
	config_for_cam1.parseParams("1");
	Camera camera1(config_for_cam1,params_all);
	
	Config_params config_for_cam2;
	config_for_cam2.parseParams("2");
	Camera camera2(config_for_cam2,params_all);
	
	camera0.start_processing();
	camera1.start_processing();
	camera2.start_processing();
	
	
	p_state_door= make_unique<thread>([params_all]()
	{
		std::cout<<"Start state_door"<<endl;
		const char *b[]={"a",params_all.door.ip.c_str(),params_all.door.port.c_str()};
		StatusDoor(3,b);	
		this_thread::sleep_for(chrono::milliseconds(1000));		
	});
	p_state_door->join();
	camera0.p_frame_processor->join();
	camera1.p_frame_processor->join();
	camera2.p_frame_processor->join();
	
	return 0;
}